from django.urls import path
from . import views

urlpatterns=[
    path('procurador/', views.ProcuradorListView.as_view(), name='procurador_list'),
    path('procurador/nuevo/', views.ProcuradorCreateView.as_view(), name='procurador_create'),
    path('procurador/<int:pk>/', views.ProcuradorUpdateView.as_view(), name='procurador_update'),
    path('procurador/<int:pk>/eliminar', views.ProcuradorDeleteView.as_view(), name='procurador_delete')

]