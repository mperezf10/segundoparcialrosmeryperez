from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.views.generic import ListView
from .models import procurador
from .forms import ProcuradorForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

# Create your views here.
class ProcuradorListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = ProcuradorListView
    template_name = 'procurador/procurador_list.html'
    context_object_name = 'procurador'
    paginate_by = 3
    login_url = reverse_lazy ('login')
    permission_required = 'catalogos.view_procurador'

class ProcuradorCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = ProcuradorCreateView
    form_class = ProcuradorForm
    template_name = 'procurador/procurador_form.html'
    success_url = reverse_lazy('procurador_list')
    login_url = ('login')
    permission_required = 'catalogos.add_procurador'

class ProcuradorUpdateView(LoginRequiredMixin, UpdateView):
    model = ProcuradorUpdateView
    form_class = ProcuradorForm
    template_name = 'procurador/procurador_form.html'
    success_url = reverse_lazy('procurador_list')
    login_url = ('login')

class ProcuradorDeleteView(LoginRequiredMixin, DeleteView):
    model = ProcuradorDeleteView
    template_name = 'procurador/procurador_confirm_delete.html'
    success_url = reverse_lazy('procurador_list')
    login_url = reverse_lazy('login')



