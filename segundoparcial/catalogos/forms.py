from django import forms
from .models import procurador

class ProcuradorForm(forms.ModelForm):
    class Meta:
        model = ProcuradorForm
        fields = ['dni', 'casos ganados', 'nombre','apellidos', 'numerocolegiado']
